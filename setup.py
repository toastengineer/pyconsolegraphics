from setuptools import setup
setup(name='PyConsoleGraphics',
      version='0.2',
      description='Portable, easy text-mode graphics',
      author='TOASTEngineer',
      author_email='toastengineer@gmail.com',
      packages=['pyconsolegraphics', 'pyconsolegraphics.backends'],
	  )
import random
import string
import time

import copy

import itertools
import pygame
import sys
import math

import pyconsolegraphics as pc


def test_print_source():
    term=pc.Terminal(font="Fixedsys Excelsior 3.01, Source Code Pro, Courier")
    cursor=pc.Cursor(term, fgcolor=None)
    cursor.autoscroll=True
    cursor.blink=True
    term.draw()

    corpus=iter(open("pyconsolegraphics\\__init__.py", "rt",
                     encoding="UTF-8").read(

    ) + ''.join([
        random.choice(string.ascii_letters+"～") for t in range(30000)]))
    #corpus = (random.choice(string.ascii_letters+"～") for t in range(30000))
    n=0
    while True:
        n+=1
        for t in range(1):
            try:
                cursor.smart_writechar(next(corpus))
            except pc.VerticalCursorOverflowError:
                cursor.pos=(0,0)
                term.blank()
            if n % 3000 == 0:
                cursor.bgcolor=(random.randint(0,255),random.randint(0,255),
                                random.randint(0,255))
                term.bgcolor=(random.randint(0,255),random.randint(0,255),
                                random.randint(0,255))
                term.fgcolor=(random.randint(0,255),random.randint(0,255),
                                random.randint(0,255))
        term.draw()
        pygame.event.pump()

def test_stdio():
    term=pc.Terminal(font="Fixedsys Excelsior 3.01, Courier")
    term.fgcolor=(255,255,255,255)
    term.bgcolor=(0,0,0,0)

    pc.autodraw(term)
    sys.stdin=sys.stdout=term

    while True:
        print(input("> "))

def test_poses():
    term=pc.Terminal(font = "Fixedsys Excelsior 3.01, Source Code Pro-Regular, "
                            "Courier")
    cursor=pc.Cursor(term, cursorchar='')
    #If we didn't have this, putting text at the bottom right corner would
    #cause the whole thing to scroll down.
    cursor.autoscroll=False

    cursor.pos = term.topleft
    print(cursor.pos, file=sys.__stdout__)
    cursor.smart_writechar('1')

    cursor.pos = term.topcenter
    print(cursor.pos, file=sys.__stdout__)
    cursor.smart_writechar('2')

    cursor.pos = term.topright
    print(cursor.pos, file=sys.__stdout__)
    cursor.smart_writechar('3')

    cursor.pos = term.centerleft
    print(cursor.pos, file=sys.__stdout__)
    cursor.smart_writechar('4')

    cursor.pos = term.center
    print(cursor.pos, file=sys.__stdout__)
    cursor.smart_writechar('5')

    cursor.pos = term.centerright
    print(cursor.pos, file=sys.__stdout__)
    cursor.smart_writechar('6')

    cursor.pos = term.bottomleft
    print(cursor.pos, file=sys.__stdout__)
    cursor.smart_writechar('7')

    cursor.pos = term.bottomcenter
    print(cursor.pos, file=sys.__stdout__)
    cursor.smart_writechar('8')

    cursor.pos = term.bottomright
    print(cursor.pos, file=sys.__stdout__)
    try:
        cursor.smart_writechar('9')
    except pc.VerticalCursorOverflowError:
        pass

    while True:
        term.draw()

def test_ezmode():
    term=pc.Terminal(font = "Fixedsys Excelsior 3.01")
    pc.autodraw(term)

    import pyconsolegraphics.ezmode
    gfx=pyconsolegraphics.ezmode.EZMode(term)

    gfx.put_line_at("Hello World", "center")
    gfx.center_line_at("Hello world!", (0,1), "center")
    gfx.put_line_at("Off the edge...", (term.width - 10, 8), allow_wrap=False)

    clist = list(pc.colors.keys())
    randcolor = lambda: random.choice(clist)
    randpos = lambda: (random.randint(0, term.width-1), random.randint(0, term.height-1))

    def colorslide():
        color = (127, 127, 127)
        while True:
            r, g, b = color
            r += random.randint(-5, 5)
            r = max(0, min(r, 255))
            g += random.randint(-5, 5)
            g = max(0, min(g, 255))
            b += random.randint(-5, 5)
            b = max(0, min(b, 255))
            color = r, g, b
            yield color
    slider = colorslide()

    hello = gfx.get_line("> ")

    while True:
        time.sleep(0.1)
        gfx.write_line(hello, fgcolor=next(slider))
        if random.randint(1, 6) == 1:
            gfx.center_line_at("Hello world!",
                pos=randpos(), bgcolor=randcolor(), allow_wrap=False)

def test_fps():
    term = pc.Terminal(font="Fixedsys Excelsior 3.01")

    import pyconsolegraphics.ezmode
    gfx = pyconsolegraphics.ezmode.EZMode(term)

    frames = 0
    lasttime = time.monotonic()

    while True:
        term.draw()
        if time.monotonic() - lasttime >= 1:
            gfx.center_line_at("FPS: {0}".format(frames), "center",
                               fgcolor="red", bgcolor="white")
            frames = 0
            term.draw()
            time.sleep(0.5)
            lasttime = time.monotonic()
        gfx.put_line("fooooooo!?", bgcolor="blue")
        frames += 1

def test_rfc1896_rich_text():
    term = pc.Terminal(
        font="Fixedsys Excelsior 3.01, Source Code Pro, Courier")
    from pyconsolegraphics import extendedcursors
    cursor=extendedcursors.RichTextCursor(term)
    cursor.autoscroll = True
    cursor.blink = True
    term.draw()

    corpus = """
Normal text...
<bold>Bold text!</bold>
<center>Centered text!</center>
<italic>Italic text!</italic>
<color=red>Rain<color=orange>bow<color=green> te<color=(11,0,255)>xt<color=violet>!</color>
</color></color></color></color>
<underline>Underlined text looks like shit for now. :(</underline>
<italic>All italic, <underline>part</underline> <bold>bold!</italic></bold>
Two <<s is the escape character for the literal <<.

We <i
t
a
l
i
c>ignore<
/
i
t
alic
> control characters like \\n inside tags.

We also want to <italic><italic>cor<italic></italic>rect</italic>ly</italic> 
handle nesting many of the same tag.

<bold><italic><underline>Or ind<italic>eed, </bold> (this part isn't bold)</ital
ic><bold><bold><underline>, handle nesting</underline> many of many different
</bold> tags</bold></underline></italic></italic>

(This text should be plain).

We can also change <color fgcolor="orangered" bgcolor="blue">background color</color>,
and can <color="olivedrab">nest <color=bisque>color</color> tags
<color=lightgoldenrod>inside <color=tomato2>other </color></color></color>ones.

Now let's go through all the colors in our database...

"""
    corpus = list(corpus)

    colorcorpus = "\t".join(
        ["<color={0}>{0}</color>".format(thename)
            for thename in sorted(pc.colors.keys())])

    corpus = list(reversed(corpus))
    colorcorpus = list(reversed(colorcorpus))

    while corpus:
        cursor.smart_writechar(corpus.pop())
        term.draw()
        if not cursor.is_reading_tag:
            time.sleep(1 / 15)
    time.sleep(2)
    while colorcorpus:
        cursor.smart_writechar(colorcorpus.pop())
        term.draw()
    time.sleep(60)

def test_clickzones():
    term = pc.Terminal(
        font="Fixedsys Excelsior 3.01, Source Code Pro, Courier")
    from pyconsolegraphics import ClickZone, colors
    from pyconsolegraphics.ezmode import EZMode
    ez = EZMode(term)

    global buttoncolor
    nohighlightcolor = buttoncolor = (64,64,64)
    highlightcolor = (127,127,127)

    c = ClickZone(term, term.center, width=6)

    term.clickzonemanager.activate_group("default")

    def on_highlight():
        print("Highlighted")
        global buttoncolor
        buttoncolor = highlightcolor
    c.on_highlight = on_highlight

    def on_lose_highlight():
        print("Unhighlighted")
        global buttoncolor
        buttoncolor = nohighlightcolor
    c.on_lose_highlight = on_lose_highlight

    def on_click():
        print("Clicked")
        term.bgcolor=(random.randint(0,255),random.randint(0,255),
                      random.randint(0,255))
    c.on_click = on_click

    while True:
        #print(term.backend.get_mouse())
        term.process()
        term.blank()
        ez.center_line_at("BUTTON", term.center, bgcolor=buttoncolor)
        try:
            term[term.backend.get_mouse()].bgcolor=(0,255,0)
        except IndexError:
            pass
        term.draw()
        time.sleep(1/75)

def test_clickzones_2():
    import pyconsolegraphics.ui
    term = pc.Terminal(
        font="Fixedsys Excelsior 3.01,Source Code Pro, Courier")

    for t in range(5):
        pyconsolegraphics.ui.TickBoxClickZone(term,
                         label = str(t),
                         bgcolor=(random.randint(0,255),random.randint(0,255),random.randint(0,255)),
                         center = (random.randint(15,60), random.randint(16,30)),
                         width = random.randint(5, 8),
                         height = random.randint(1, 3))
    pyconsolegraphics.ui.SpinnerClickZone(term, term.topcenter,
                                          ["aardvark", "beta fish",
                                           "coleocanth", "duck"])
    i=pyconsolegraphics.ui.IncrementDecrementClickZone(term,
                                                       term.bottomcenter,
                                                       increment = 15,
                                                       maxval=100,
                                                       minval=-100,
                                                       label="{value}%")

    term.clickzonemanager.activate_group("default")

    while True:
        try:
            term.process()
        except pyconsolegraphics.OverlappingZoneError:
            return
        #term.blank()
        try:
            m = term.backend.get_mouse(topleft=False)
            print(m)
            rm = math.floor(m[0]), math.floor(m[1])
            term.backend.surface.set_at(rm, (255,0,0))
            pm = term.backend.cell_to_px(m)
            term.backend.surface.set_at(pm, (255, 0, 255))
            #term[rm].bgcolor = (0, 255, 0)
        except IndexError:
            pass
        term.draw()

def test_clickzones_3():
    term = pc.Terminal(
        font="Fixedsys Excelsior 3.01,Source Code Pro, Courier")
    from pyconsolegraphics import ui
    ui.VerticalMenu({
        "Print __init__.py" : test_print_source,
        "Test stdio replacement" : test_stdio,
        "Label terminal's position attributes" : test_poses,
        "Test ezmode API" : test_ezmode,
        "Fill terminal and display FPS" : test_fps,
        "Test rich text cursor" : test_rfc1896_rich_text,
        "Clickzone test 1" : test_clickzones,
        "Clickzone test 2" : test_clickzones_2,
        "Clickzone test 3 (this menu!)" : test_clickzones_3,
        "Test slice interface support of Terminal object" : test_terminal_slicing,
        "Test coordinate space translation": test_px_to_cell,
    }, term, group="menu1")
    term.clickzonemanager.activate_group("menu1")

    while True:
        term.process()
        term.draw()

def test_terminal_slicing():
    term = pc.Terminal(
        font="Fixedsys Excelsior 3.01,Source Code Pro, Courier")
    rect1 = term[:(5,5)]

    rect2 = term[(6,5) : (8, 12)]

    rect3 = term[(10,10) : (20,20) : (2, 3)]

    rect4 = list(term[::5])

    while True:
        if rect4:
            rect4.pop().character="4"
        if rect3:
            rect3.pop().character="3"
        if rect2:
            rect2.pop().character="2"
        if rect1:
            rect1.pop().character="1"
        term.draw()
        time.sleep(0.25)

def test_px_to_cell():
    #Tests that the conversions between pixel-space and terminal-space
    #look right by moving one point in each coordinate system and drawing
    #it in both. Both 1-pixel dots should move smoothly and always be
    #inside the displayed cell. Note that they might LOOK wrong at the edges if
    #you look too close to your screen because of the way the individual
    #color elements are arranged; use Windows Magnifier or take screenshots
    # instead.
    term = pc.Terminal(
        font="Fixedsys Excelsior 3.01,Source Code Pro, Courier")
    ball1pos = term.center
    ball2pos = term.backend.cell_to_px(term.center)

    ball1vel = pc.Pos(0.05,0.07)
    ball2vel = pc.Pos(1,-1)

    while True:
        term.blank()

        ball1pos += ball1vel
        print(ball1pos, ball1vel, ball2pos, ball2vel)
        if not (0, 0) <= ball1pos <= term.bottomright:
            ball1vel = -ball1vel
            ball1vel += ((random.random() - 0.5) * 0.1, (random.random() -
                                                         0.5) \
                        *0.1)
            ball1pos += (ball1vel * 2)

        term[ball1pos].character = 'o'
        term[ball1pos].fgcolor = "darkgreen"
        ball1pxpos = term.backend.cell_to_px(ball1pos)

        ball2pos += ball2vel
        if not (1, 1) <= ball2pos < term.backend.surface.get_size():
            ball2vel = -ball2vel
            ball2pos += (ball2vel * 2)

        term[term.backend.px_to_cell(ball2pos)].bgcolor="darkred"

        term.process()
        term.draw(noflip=True)
        print(ball1pos, ball1vel, ball2pos, ball2vel)
        term.backend.surface.set_at(ball1pxpos, (255, 0, 255))
        term.backend.surface.set_at(ball2pos, (0, 0, 255))
        pygame.display.flip()




#test_stdio()
#test_print_source()
#test_ezmode()
#test_fps()
#test_rfc1896_rich_text()
#test_clickzones()
#test_clickzones_2()
test_clickzones_3()
#test_terminal_slicing()
#test_px_to_cell()
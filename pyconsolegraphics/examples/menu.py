import collections

import time

import pyconsolegraphics
import pyconsolegraphics.ezmode

class Menu(collections.OrderedDict):
    """Implements a selectable menu of options, given names and callbacks. The menu items can then be selected with
    keyboard or mouse. Acts as a dict mapping labels to callbacks."""

    def __init__(self,*args,repeat=False):
        super().__init__(*args)
        self.selection=None
        self.itemys={}
        self.repeat=repeat

    def option(self,name):
        """Intended to be used as a decorator. Use like this:
        @mymenu.option("Engage self-destruct!")
        def selfdestruct():
            ..."""
        def inner(func):
            self[name]=func
        return inner

    def increment_selection(self):
        "Move the selector down one option. Wraps around to the first option."
        optionlist=list(self.keys())
        idx=optionlist.index(self.selection)
        if idx+1 >= len(self):
            self.selection = optionlist[0]
        else:
            self.selection=optionlist[idx+1]

    def decrement_selection(self):
        "Move the selector up one option. Wraps around to the last option."
        optionlist=list(self.keys())
        idx=optionlist.index(self.selection)
        if idx-1 < 0:
            self.selection=optionlist[-1]
        else:
            self.selection=optionlist[idx-1]

    def draw(self, gfx:pyconsolegraphics.ezmode.EZMode, predrawcallback,
             postdrawcallback):
        time.sleep(1 / 30)
        x, y = 5, 5
        gfx.blank()

        if predrawcallback:
            predrawcallback(gfx)
            #If the callback printed lines we should print our stuff under it
            y = gfx.get_cursor_pos()[1]

        invertedbgcolor = gfx.terminal.fgcolor
        invertedfgcolor = gfx.terminal.bgcolor

        for theoption in self.keys():
            if theoption==self.selection:

                gfx.put_line_at(theoption, (x + 1, y),
                                fgcolor=invertedfgcolor,
                                bgcolor=invertedbgcolor,
                                allow_wrap=False)
            else:
                gfx.put_line_at(theoption, (x, y), allow_wrap=False)
            y+=1

            self.itemys[y]=theoption
            #gfx.write_line(theoption + "\n", bgcolor=bgcolor, fgcolor=fgcolor)
            #gfx.put_cursor((6, gfx.get_cursor_pos()[1]))

        #gfx.update()

        if postdrawcallback:
            gfx.put_cursor((x, y+2))
            postdrawcallback(gfx)

    def select(self, gfx, **kwargs):
        """If the currently selected option is callable, call it and return its return value. Otherwise just return it."""
        selected=self[self.selection]
        if isinstance(selected, Menu):
            return selected(gfx, **kwargs)
        elif callable(selected):
            return selected(**kwargs)
        else:
            return selected

    def __call__(self, gfx : pyconsolegraphics.ezmode.EZMode, defaultselection=None,
                 predrawcallback=None,
                 postdrawcallback=None, **kwargs):
        """Make this menu active and allow it to intercept all user input. Does not exit until the menu is exited by
        pressing escape. If the object mapped to the selected option is callable, call it and return its return value;
        otherwise, return it.

        window is a pygcurse window. predrawcallback is a function that takes a pygcurse window; it gets called right after
        the menu blanks the screen and before it draws the options. postdrawcallback is similar except it gets called right after
        the options are drawn and Pygcurse is asked to update the screen (so call window.update() after your changes).
        Any given keyword arguments are passed into contained callables."""
        if len(self) == 0:
            raise ValueError("Tried to activate an empty menu")

        self.selection=defaultselection
        if self.selection is None:
            self.selection=list(self.keys())[0]

        while True:

            self.draw(gfx, predrawcallback, postdrawcallback)

            for key in gfx.terminal.backend.get_keypresses():

                if key=="escape":
                    return

                elif key=="down":
                    self.increment_selection()

                elif key=="up":
                    self.decrement_selection()

            for character in gfx.terminal.backend.get_characters():
                if character in ("\n", "\t", " "):
                    result=self.select(gfx, **kwargs)
                    if not self.repeat:
                        return result

            x, y = term.backend.get_mouse()
            try:
                self.selection=self.itemys[y]
            except KeyError:
                pass

            if term.backend.get_left_click():
                result=self.select(gfx, **kwargs)
                if not self.repeat:
                    return result


themenu = Menu(repeat=True)

@themenu.option("Foo")
def foo():
    print("Foo")

@themenu.option("Bar")
def bar():
    print("Bar")

othermenu = Menu()
themenu["Xyzzy"] = othermenu

@themenu.option("Baz")
def baz():
    print("Baz")
    exit()

@othermenu.option("Plugh")
def plugh():
    print ("plugh")

@othermenu.option("Frotz")
def frotz():
    print("frotz")

if __name__ == "__main__":
    term = pyconsolegraphics.Terminal(font = "Fixedsys Excelsior 3.01, Courier")
    pyconsolegraphics.autodraw(term)
    gfx = pyconsolegraphics.ezmode.EZMode(term)
    themenu(gfx)
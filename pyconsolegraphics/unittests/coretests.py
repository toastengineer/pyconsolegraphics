import hashlib
import string
import unittest
from hypothesis import *
from hypothesis.strategies import *
import pyconsolegraphics as pc
import pygame
import unittest
from math import *
from pyconsolegraphics.unittests.image_comparison import \
    difference_from_previous_revision

class CoreTests(unittest.TestCase):
    def setUp(self):
        #This shouldn't be here really,
        #I guess there should be a "test core features"
        #on every backend... maybe have the tests
        #on this object and then subclass it for every
        #backend? For now we just assume pygame.
        pygame.init()

    def tearDown(self):
        pygame.quit()

    def test_simple_initialization(self):
        pc.Terminal()

    @given(text())
    def test_print_string(self, corpus):
        term = pc.Terminal()
        cursor = pc.Cursor(term)

        for thechar in corpus:
            cursor.smart_writechar(thechar)

        term.process()
        term.draw()

    #Might be redundant but since Hypothesis doesn't run _that_ many test cases
    #I wanted one that was focused specifically on ASCII and especially
    #control codes.
    @given(text(alphabet= [chr(x) for x in range(128)], average_size=1000))
    #@settings(max_examples=500)
    def test_print_ascii_string(self, corpus):
        term = pc.Terminal()
        cursor = pc.Cursor(term)

        for thechar in corpus:
            cursor.smart_writechar(thechar)

        term.process()
        term.draw()

        try:
            diff = difference_from_previous_revision(
                term.backend.surface, "test_print_ascii_string",
                (hashlib.md5(corpus.encode()).hexdigest(),),
                error_on_no_previous=True)
        except FileNotFoundError:
            assume(False)

        self.assertIsNone(diff, "Difference from previous revision detected.")

    @given(integers(), integers(), booleans())
    def test_px_to_cell(self, x, y, topleft):
        term = pc.Terminal()
        cx, cy = term.backend.px_to_cell((x,y), topleft)
        assert 0 <= cx < term.width, (cx, cy)
        assert 0 <= cy < term.height, (cx, cy)

        if not topleft and pc.Pos(0,0) < pc.Pos(x, y) < term.backend.surface.get_size():
            checkx, checky = term.backend.cell_to_px((cx, cy))
            self.assertAlmostEqual(x, checkx, delta=term.backend.cellwidth-1, msg=(cx,cy))
            self.assertAlmostEqual(y, checky, delta=term.backend.cellwidth-1, msg=(cx,cy))

    @given(floats(allow_infinity=False, allow_nan=False), floats(allow_infinity=False, allow_nan=False))
    def test_cell_to_px(self, x, y):
        term = pc.Terminal()
        px, py = term.backend.cell_to_px((x,y)) #type: (float, float)

        assert 0 <= px < term.backend.surface.get_width(), (px,py)
        assert 0 <= py < term.backend.surface.get_height(), (px,py)
        #if the cell is out of range, they won't match because we clamp it to
        # be in range - so don't bother
        if pc.Pos(0,0) < pc.Pos(x, y) < term.size:
            checkx, checky = term.backend.px_to_cell((px, py))
            self.assertAlmostEqual(x, checkx, delta=term.backend.cellwidth, msg=(px,py))
            self.assertAlmostEqual(y, checky, delta=term.backend.cellwidth, msg=(px,py))
